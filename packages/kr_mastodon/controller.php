<?php      
namespace Concrete\Package\KRMastodon;
use Package;
use BlockType;
use View;
use Loader;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends Package {

	protected $pkgHandle = 'kr_mastodon';
	protected $appVersionRequired = '5.7.4';
	protected $pkgVersion = '0.0.1';
			
 	
	public function getPackageName() 
	{
		return t("Mastodon");
	}

	public function getPackageDescription() 
	{
		return t("Masotodon");
	}

	public function install() 
	{
		$pkg = parent::install();

		// install block
		BlockType::installBlockTypeFromPackage('kr_mastodon', $pkg);
		
		return $pkg;
	}
	public function uninstall() {
		parent::uninstall();
	}

}