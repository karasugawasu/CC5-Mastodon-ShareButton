<?php  defined('C5_EXECUTE') or die("Access Denied."); ?>
<?php
	$page = Page::getCurrentPage();
	$pagename = $page->getCollectionName();
	$pagelink = $page->getCollectionLink();
	$sitename = "[" . Core::make('site')->getSite()->getSiteName() . "]";
	
	$urlHelper = Core::make('helper/concrete/urls');
	$blockType = BlockType::getByHandle('kr_mastodon');
	$localPath = $urlHelper->getBlockTypeAssetsURL($blockType);

	echo "<a href=\"web+mastodon://share?text=" . urlencode($sitename . $pagename . "\n" . $pagelink) . "\" onclick=\"window.open(this.href, '', 'width=500,height=400'); return false;\"><img src=\"" . BASE_URL . $localPath . "/toot.png\" width=\"100\" height=\"21\"></a>"
?>