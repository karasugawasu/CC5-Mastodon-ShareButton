<?php          
namespace Concrete\Package\KRMastodon\Block\KRMastodon;
use Package;
use View;
use Loader;
use Page;
use Core;
use \Concrete\Core\Block\BlockController;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends BlockController {
	
	protected $btCacheBlockRecord = false;
	protected $btCacheBlockOutput = false;
	protected $btCacheBlockOutputOnPost = false;
	protected $btCacheBlockOutputForRegisteredUsers = false;

	public function getBlockTypeDescription()
    {
        return t("MasotodonShare");
    }

    public function getBlockTypeName()
    {
        return t("MastodonShare");
    }



}
?>